﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Security.Cryptography;

namespace Pass_Store
{
    public partial class Login_screen : Form
    {
        public Login_screen()
        {
            InitializeComponent();
        }

        private void btn_prijava_Click(object sender, EventArgs e)
        {
			//dobi vnešene podatke
            var upime = txt_upime.Text;
            var geslo = txt_geslo.Text;

            if(preveriLogin(geslo, upime)) {
                //naloži 2. formo, ki je core programa
                var mainForm = new Main_form();
				this.Hide();
				mainForm.Show();
			} else {
                MessageBox.Show("Uporabniško ime ali geslo je napačno!", "Napaka v prijavi!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

		//preveri če je podano geslo in up.ime pravilno
        private bool preveriLogin(String v_geslo, String v_upime)
        {
            string sql_login = "SELECT UPIME, GESLO FROM USERS";
			ConnectDB baza = new ConnectDB();
			SQLiteDataReader bralec = baza.izvediQuery(sql_login);

            Kodirnik kod = new Kodirnik(v_geslo);
            string hashedGeslo = kod.vrniHash();

            while (bralec.Read()){
                if(hashedGeslo.Equals(bralec["GESLO"]) && v_upime.Equals(bralec["UPIME"])){
					baza.zapriPovezavo();
                    return true;
                }
            }
			baza.zapriPovezavo();
			return false;
        }

        private void Login_screen_Shown(object sender, EventArgs e)
        {
            txt_upime.Focus();
        }

        private void txt_geslo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_prijava.PerformClick();
            }
        }

		private void Login_screen_FormClosing(object sender, FormClosingEventArgs e) {
			System.Windows.Forms.Application.Exit();
		}
	}
}
