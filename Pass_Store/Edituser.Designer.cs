﻿using System.Data.SQLite;

namespace Pass_Store
{
    partial class Edituser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Edituser));
			this.cur_geslo = new System.Windows.Forms.TextBox();
			this.add_user_btn = new System.Windows.Forms.Button();
			this.edit_upime = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.new_geslo = new System.Windows.Forms.TextBox();
			this.new_geslo_confirm = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// cur_geslo
			// 
			this.cur_geslo.Location = new System.Drawing.Point(117, 64);
			this.cur_geslo.Name = "cur_geslo";
			this.cur_geslo.PasswordChar = '*';
			this.cur_geslo.Size = new System.Drawing.Size(155, 20);
			this.cur_geslo.TabIndex = 5;
			// 
			// add_user_btn
			// 
			this.add_user_btn.Location = new System.Drawing.Point(78, 181);
			this.add_user_btn.Name = "add_user_btn";
			this.add_user_btn.Size = new System.Drawing.Size(118, 23);
			this.add_user_btn.TabIndex = 4;
			this.add_user_btn.Text = "Spremeni uporabnika";
			this.add_user_btn.UseVisualStyleBackColor = true;
			this.add_user_btn.Click += new System.EventHandler(this.add_user_btn_Click);
			// 
			// edit_upime
			// 
			this.edit_upime.Location = new System.Drawing.Point(117, 37);
			this.edit_upime.Name = "edit_upime";
			this.edit_upime.Size = new System.Drawing.Size(155, 20);
			this.edit_upime.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(19, 40);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(89, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "Uporabniško ime:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(27, 67);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(81, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Trenutno geslo:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(44, 115);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Novo geslo:";
			// 
			// new_geslo
			// 
			this.new_geslo.Location = new System.Drawing.Point(117, 112);
			this.new_geslo.Name = "new_geslo";
			this.new_geslo.PasswordChar = '*';
			this.new_geslo.Size = new System.Drawing.Size(155, 20);
			this.new_geslo.TabIndex = 9;
			// 
			// new_geslo_confirm
			// 
			this.new_geslo_confirm.Location = new System.Drawing.Point(117, 139);
			this.new_geslo_confirm.Name = "new_geslo_confirm";
			this.new_geslo_confirm.PasswordChar = '*';
			this.new_geslo_confirm.Size = new System.Drawing.Size(155, 20);
			this.new_geslo_confirm.TabIndex = 10;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(7, 142);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(101, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Potrdite novo geslo:";
			// 
			// Edituser
			// 
			this.AcceptButton = this.add_user_btn;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.new_geslo_confirm);
			this.Controls.Add(this.new_geslo);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cur_geslo);
			this.Controls.Add(this.add_user_btn);
			this.Controls.Add(this.edit_upime);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Edituser";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Spremeni uporabnika";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Edituser_FormClosing);
			this.Shown += new System.EventHandler(this.Edituser_Shown);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Edituser_KeyDown);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox cur_geslo;
        private System.Windows.Forms.Button add_user_btn;
        private System.Windows.Forms.TextBox edit_upime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox new_geslo;
        private System.Windows.Forms.TextBox new_geslo_confirm;
        private System.Windows.Forms.Label label4;
    }
}