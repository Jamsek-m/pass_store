﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pass_Store
{
    public partial class New_Entry : Form
    {
        public New_Entry()
        {
            InitializeComponent();
        }

        private void add_entry_btn_Click(object sender, EventArgs e)
        {
			//vrednosti iz forme
            string naziv = i_naziv.Text;
            string upime = i_upime.Text;
            string geslo = i_geslo.Text;
            string email = i_email.Text;
            string sec_q = i_sec_q.Text;
            string sec_a = i_sec_a.Text;
            string date_birth = i_date_birth.Text;
            string notes = i_notes.Text;

			//kriptira vrednosti z Kripto razredom
			Kripto enkript = new Kripto();
			string enc_naziv = enkript.Encrypt(naziv, false);
			string enc_upime = enkript.Encrypt(upime, false);
			string enc_geslo = enkript.Encrypt(geslo, false);
			string enc_email = enkript.Encrypt(email, false);
			string enc_sec_q = enkript.Encrypt(sec_q, false);
			string enc_sec_a = enkript.Encrypt(sec_a, false);
			string enc_date_birth = enkript.Encrypt(date_birth, false);
			string enc_notes = enkript.Encrypt(notes, false);

			//preveri da je vnešen vsaj en podatek - vrne true, če je prazna forma
			if (praznaForma(naziv, upime, geslo, email, sec_a, sec_q, date_birth, notes)){
                MessageBox.Show("Vsa polja so prazna!\nZa shrambo mora biti vnešeno vsaj eno polje.", "Prazna polja!", MessageBoxButtons.OK);
            } else {
                string sql = "insert into SAVED_PASS (UPIME, GESLO, EMAIL, NAZIV, SEC_Q, SEC_A, DATE_BIRTH, NOTES) values ('" +
                    enc_upime + "','" +
					enc_geslo + "','" +
					enc_email + "','" +
					enc_naziv + "','" +
					enc_sec_q + "','" +
					enc_sec_a + "','" +
					enc_date_birth + "','" +
					enc_notes + "')";
                ConnectDB baza = new ConnectDB();
                baza.izvediNonQuery(sql);
				baza.zapriPovezavo();
				//se vrne na mainForm
				pojdiNazaj();
            }
        }

		protected void pojdiNazaj() {
			this.Close();
		}

		//preveri če je dolžina podanih parametrov večja od 0
		//če so vsi nič (prazna forma) vrne true
        public static bool praznaForma(string param1, string param2, string param3, string param4, string param5, string param6, string param7, string param8 ) {
            if(param1.Length > 0 || param2.Length > 0 || param3.Length > 0 || param4.Length > 0 || param5.Length > 0 || param6.Length > 0 || param7.Length > 0 || param8.Length > 0)
            {
                return false;
            }
            return true;
        }

		private void New_Entry_FormClosing(object sender, FormClosingEventArgs e) {
			var mainForm = new Main_form();
			mainForm.Show();
		}
	}
}
