﻿namespace Pass_Store
{
    partial class Login_screen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login_screen));
			this.btn_prijava = new System.Windows.Forms.Button();
			this.txt_upime = new System.Windows.Forms.TextBox();
			this.txt_geslo = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// btn_prijava
			// 
			this.btn_prijava.Location = new System.Drawing.Point(150, 120);
			this.btn_prijava.Name = "btn_prijava";
			this.btn_prijava.Size = new System.Drawing.Size(75, 25);
			this.btn_prijava.TabIndex = 0;
			this.btn_prijava.Text = "Prijava";
			this.btn_prijava.UseVisualStyleBackColor = true;
			this.btn_prijava.Click += new System.EventHandler(this.btn_prijava_Click);
			// 
			// txt_upime
			// 
			this.txt_upime.Location = new System.Drawing.Point(120, 35);
			this.txt_upime.Name = "txt_upime";
			this.txt_upime.Size = new System.Drawing.Size(200, 20);
			this.txt_upime.TabIndex = 1;
			// 
			// txt_geslo
			// 
			this.txt_geslo.Location = new System.Drawing.Point(120, 70);
			this.txt_geslo.Name = "txt_geslo";
			this.txt_geslo.PasswordChar = '*';
			this.txt_geslo.Size = new System.Drawing.Size(200, 20);
			this.txt_geslo.TabIndex = 2;
			this.txt_geslo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_geslo_KeyDown);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(20, 35);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(89, 13);
			this.label1.TabIndex = 3;
			this.label1.Text = "Uporabniško ime:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(75, 75);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(37, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Geslo:";
			// 
			// Login_screen
			// 
			this.AcceptButton = this.btn_prijava;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(375, 160);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.txt_geslo);
			this.Controls.Add(this.txt_upime);
			this.Controls.Add(this.btn_prijava);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Login_screen";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Prijava";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Login_screen_FormClosing);
			this.Shown += new System.EventHandler(this.Login_screen_Shown);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_prijava;
        private System.Windows.Forms.TextBox txt_upime;
        private System.Windows.Forms.TextBox txt_geslo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
	}
}

