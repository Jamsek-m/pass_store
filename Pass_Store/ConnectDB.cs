﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Security.Cryptography;
using System.IO;

namespace Pass_Store
{
    public class ConnectDB
    {
        private SQLiteConnection db_Connect;
		//pot do lokalnega appData, kjer se shrani baza
		private static string path = Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Pass_Store\\geslaDB.sqlite";
		//dataSource za bazo
		private string dataSrc = "Data Source=" + path + ";Version=3;";

        public ConnectDB()
        {
			try {
				db_Connect = new SQLiteConnection(dataSrc);
				db_Connect.Open();
				if (preveriBazo()) {
					db_Connect.Close();
					initDB();
				}
			} catch(SQLiteException e) {
				initDB();
			}

		}

		//če baza ni ustvarjena, jo ustvari, ter kreira tabele in uporabnika
		protected void initDB() {
			string createUkaz_Saved_Pass = "CREATE TABLE IF NOT EXISTS SAVED_PASS (ID_STORE INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , UPIME VARCHAR, GESLO VARCHAR, EMAIL VARCHAR, NAZIV VARCHAR, SEC_Q VARCHAR, SEC_A VARCHAR, DATE_BIRTH VARCHAR, NOTES VARCHAR)";
			string createUkaz_Users = "CREATE TABLE IF NOT EXISTS USERS (ID_USR INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , UPIME VARCHAR NOT NULL , GESLO VARCHAR NOT NULL )";
			string insertUkaz = "INSERT INTO USERS VALUES(null,'root','e8cc564a5e9320d6c22647c5e6dab55005bf1e68')";

			kreirajMapo();
			//preveri če file obstaja, če ne, potem ga ustvari
			if (!File.Exists(path)) {
				SQLiteConnection.CreateFile(path);
			}
			//odpre povezavo do file-a
			db_Connect = new SQLiteConnection(dataSrc);
			db_Connect.Open();

			//kreira tabele
			SQLiteCommand ukazUstvari_SP = new SQLiteCommand(createUkaz_Saved_Pass, db_Connect);
			ukazUstvari_SP.ExecuteNonQuery();
			SQLiteCommand ukazUstvari_US = new SQLiteCommand(createUkaz_Users, db_Connect);
			ukazUstvari_US.ExecuteNonQuery();

			//IF table empty - vstavi userja
			if (preveriVnose()) {
				SQLiteCommand ukazVstavi = new SQLiteCommand(insertUkaz, db_Connect);
				ukazVstavi.ExecuteNonQuery();
			}
		}

		//preveri ali je kakšen user že dodan
		protected bool preveriVnose() {
			string sql = "select COUNT(UPIME) as VNOSOV from USERS";
			SQLiteCommand ukaz = new SQLiteCommand(sql, db_Connect);
			SQLiteDataReader bralec = ukaz.ExecuteReader();
			string izpis = bralec["VNOSOV"].ToString();
			int vnosov = Int32.Parse(izpis);

			if (vnosov == 0) {
				return true;
			}
			return false;
		}

		//preveri ali ima baza kakšno tabelo
		protected bool preveriBazo() {
			string sql = "select count(name) as ST_BAZ from SQLITE_MASTER where NAME!='sqlite_sequence'";
			SQLiteCommand ukaz = new SQLiteCommand(sql, db_Connect);
			SQLiteDataReader bralec = ukaz.ExecuteReader();
			int obstojecihBaz = Int32.Parse(bralec["ST_BAZ"].ToString());
			if (obstojecihBaz == 0) {
				return true;
			}
			return false;
		}

		//izvede non-query poizvedbo (insert, update, delete,..)
		public void izvediNonQuery(string sql)
        {
            SQLiteCommand ukaz = new SQLiteCommand(sql, db_Connect);
            ukaz.ExecuteNonQuery();
        }

		//izvede query poizvedbo (select) in vrne objekt bralec, ki bere iz baze
        public SQLiteDataReader izvediQuery(string sql)
        {
            SQLiteCommand ukaz = new SQLiteCommand(sql, db_Connect);
            SQLiteDataReader bralec = ukaz.ExecuteReader();
            return bralec;
        }

		//zapre povezavo do baze
		public void zapriPovezavo() {
			db_Connect.Close();
		}

		//preveri če dir obstaja, in če ne, jo kreira
		public void kreirajMapo() {
			string lokacijaMape = Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Pass_Store";
			if (Directory.Exists(lokacijaMape)) {
				return;
			}
			DirectoryInfo di = Directory.CreateDirectory(lokacijaMape);
		}

    }

    class Kodirnik
    {
		/* Razred za kodiranje gesel za dostop do programa
		 * uporablja enkripcijo SHA1
		 */
        private string hashedValue;

        public Kodirnik(string toHash)
        {
            hashedValue = Hash(toHash);
        }

		//getter za hash vrednost
        public string vrniHash()
        {
            return hashedValue;
        }

		//vzame string iz objekta in ga hasha
        public static string Hash(string input)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(input));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }
    }
}
