﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Pass_Store
{
    public partial class Edituser : Form
    {
        public Edituser()
        {
            InitializeComponent();
			edit_upime.Text = vrniCurrentUser();
        }

		//vrne trenutnega uporabnika iz baze
		public static string vrniCurrentUser() {
			string currentUser = "";
			ConnectDB baza = new ConnectDB();
			string sql = "select UPIME from USERS";
			SQLiteDataReader bralec = baza.izvediQuery(sql);
			while (bralec.Read()) {
				currentUser = bralec["UPIME"].ToString();
			}
			baza.zapriPovezavo();
			return currentUser;
		}

		//posodobi up.ime in geslo v bazi
		private void add_user_btn_Click(object sender, EventArgs e) {
            ConnectDB baza = new ConnectDB();
            string upime = edit_upime.Text;
            string curGeslo = cur_geslo.Text;
            string newGeslo = new_geslo.Text;
            string confGeslo = new_geslo_confirm.Text;

			if (newGeslo.Equals("")) {
				//posodobi samo upime
				Kodirnik cur_kod = new Kodirnik(curGeslo);
				string cur_hashedGeslo = cur_kod.vrniHash();
				if (pravilnoGeslo(baza, cur_hashedGeslo)) {
					string sql = "update USERS set UPIME='" + upime + "'";
					baza.izvediNonQuery(sql);
					MessageBox.Show("Uporabniško ime je bilo spremenjeno.", "Uspeh!", MessageBoxButtons.OK);
					baza.zapriPovezavo();
					pojdiNazaj();
				} else {
					MessageBox.Show("Trenutno geslo je napačno!", "Napačno geslo!", MessageBoxButtons.OK);
				}

			} else {
				if (seUjemata(newGeslo, confGeslo)) {
					Kodirnik cur_kod = new Kodirnik(curGeslo);
					string cur_hashedGeslo = cur_kod.vrniHash();
					if (pravilnoGeslo(baza, cur_hashedGeslo)) {

						Kodirnik new_kod = new Kodirnik(newGeslo);
						string new_hashedGeslo = new_kod.vrniHash();

						string sql = "update USERS set UPIME='" +
						upime + "', GESLO='" +
						new_hashedGeslo + "'";
						baza.izvediNonQuery(sql);

						MessageBox.Show("Uporabniško ime in geslo sta bila spremenjena.", "Uspeh!", MessageBoxButtons.OK);
						baza.zapriPovezavo();
						pojdiNazaj();
					} else {
						MessageBox.Show("Trenutno geslo je napačno!", "Napačno geslo!", MessageBoxButtons.OK);
					}
				} else {
					MessageBox.Show("Gesli se ne ujemata!", "Napaka!", MessageBoxButtons.OK);
				}
			}
			
        }

		public void pojdiNazaj() {
			this.Close();
			
		}

		//preveri če je podano geslo pravilno
        public static bool pravilnoGeslo(ConnectDB baza, string v_geslo)
        {
            string sql = "select GESLO from USERS";
            SQLiteDataReader bralec = baza.izvediQuery(sql);

            while (bralec.Read())
            {
                if (bralec["GESLO"].Equals(v_geslo))
                {
                    return true;
                }
            }
            return false;

        }

		//preveri če se novi gesli ujemata (za potrditev)
        public static bool seUjemata(string geslo, string geslo_confirm)
        {
            if (geslo.Equals(geslo_confirm))
            {
                return true;
            }
            return false;
        }
		
		private void Edituser_KeyDown(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Enter) {
				add_user_btn.PerformClick();
			}
		}

		private void Edituser_Shown(object sender, EventArgs e) {
			cur_geslo.Focus();
		}

		private void Edituser_FormClosing(object sender, FormClosingEventArgs e) {
			var mainForm = new Main_form();
			mainForm.Show();
		}
	}
}
