﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Pass_Store
{
    public partial class Main_form : Form
    {
        public Main_form()
        {
            InitializeComponent();
			fillData();
        }

		//napolni DataGridView z podatki iz baze
		private void fillData() {
			ConnectDB baza = new ConnectDB();
			string sql = "select * from SAVED_PASS";
			SQLiteDataReader bralec = baza.izvediQuery(sql);
			DataEncryptor keys = new DataEncryptor();
			
			while (bralec.Read()) {
				//dekriptira podatke iz baze, da jih lahko prikaze
				Kripto enkript = new Kripto();
				string id_store = bralec["ID_STORE"].ToString(); 
				string naziv = enkript.Decrypt(bralec["NAZIV"].ToString(), false);
				string upime = enkript.Decrypt(bralec["UPIME"].ToString(), false);
				string geslo = enkript.Decrypt(bralec["GESLO"].ToString(), false);
				string email = enkript.Decrypt(bralec["EMAIL"].ToString(), false);
				string sec_q = enkript.Decrypt(bralec["SEC_Q"].ToString(), false);
				string sec_a = enkript.Decrypt(bralec["SEC_A"].ToString(), false);
				string date_birth = enkript.Decrypt(bralec["DATE_BIRTH"].ToString(), false);
				string notes = enkript.Decrypt(bralec["NOTES"].ToString(), false);

				//nastavi DataGridView na podane podatke
				data_izpis.Rows.Add(new object[] {
					naziv,
					upime,
					geslo,
					email,
					sec_q,
					sec_a,
					date_birth,
					notes,
					id_store
			});
			}
			baza.zapriPovezavo();
		}

		//skrbi za navigacijo po aplikaciji
		#region navigacija
		//odpre New_Entry formo
		private void new_entry_btn_Click(object sender, EventArgs e)
        {
            var newEntry = new New_Entry();
            this.Hide();
            newEntry.Show();
        }

		//odpre EditUser formo
        private void add_user_btn_Click(object sender, EventArgs e)
        {
            var edituser = new Edituser();
            this.Hide();
            edituser.Show();
        }

		//se izvede ko zapremo glavno formo (izhod iz programa)
		private void izhod_event(object sender, FormClosingEventArgs e) {
			System.Windows.Forms.Application.Exit();
		}
		#endregion

		//ob kliku na eno izmed vrstic, se podatki posredujejo novi formi EntryDetail
		private void data_izpis_CellClick(object sender, DataGridViewCellEventArgs e) {
			string naziv = data_izpis.SelectedRows[0].Cells["NAZIV"].Value.ToString();
			string upime = data_izpis.SelectedRows[0].Cells["UPIME"].Value.ToString();
			string geslo = data_izpis.SelectedRows[0].Cells["GESLO"].Value.ToString();
			string email = data_izpis.SelectedRows[0].Cells["EMAIL"].Value.ToString();
			string sec_q = data_izpis.SelectedRows[0].Cells["SEC_Q"].Value.ToString();
			string sec_a = data_izpis.SelectedRows[0].Cells["SEC_A"].Value.ToString();
			string date_birth = data_izpis.SelectedRows[0].Cells["DATE_BIRTH"].Value.ToString();
			string notes = data_izpis.SelectedRows[0].Cells["NOTES"].Value.ToString();
			string id_store = data_izpis.SelectedRows[0].Cells["ID_STORE"].Value.ToString();

			var detail = new EntryDetail(naziv, upime, geslo, email, sec_q, sec_a, date_birth, notes, id_store);
			this.Hide();
			detail.Show();

		}

		private void about_btn_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
			var about = new About();
			about.Show();
		}
	}
}
