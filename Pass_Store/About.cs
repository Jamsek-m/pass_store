﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pass_Store {
	public partial class About : Form {
		public About() {
			InitializeComponent();
			string lokacija = Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Pass_Store";
			txt_lokacija.Text = lokacija;
		}

		private void About_FormClosing(object sender, FormClosingEventArgs e) {
		}

		private void button1_Click(object sender, EventArgs e) {
			this.Close();
		}
	}
}
