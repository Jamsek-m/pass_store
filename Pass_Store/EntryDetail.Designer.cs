﻿namespace Pass_Store {
	partial class EntryDetail {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EntryDetail));
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.i_upime = new System.Windows.Forms.TextBox();
			this.i_geslo = new System.Windows.Forms.TextBox();
			this.i_email = new System.Windows.Forms.TextBox();
			this.i_sec_q = new System.Windows.Forms.TextBox();
			this.i_sec_a = new System.Windows.Forms.TextBox();
			this.i_date_birth = new System.Windows.Forms.TextBox();
			this.i_notes = new System.Windows.Forms.TextBox();
			this.i_naziv = new System.Windows.Forms.TextBox();
			this.edit_btn = new System.Windows.Forms.Button();
			this.delete_btn = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(48, 79);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(89, 13);
			this.label2.TabIndex = 9;
			this.label2.Text = "Uporabniško ime:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(100, 108);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(37, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "Geslo:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(99, 137);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(38, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "E-mail:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(30, 168);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(107, 13);
			this.label5.TabIndex = 12;
			this.label5.Text = "Varnostno vprašanje:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(29, 201);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(108, 13);
			this.label6.TabIndex = 13;
			this.label6.Text = "Odgovor na var. vpr.:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(65, 232);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(72, 13);
			this.label7.TabIndex = 14;
			this.label7.Text = "Datum rojstva";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(87, 265);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(50, 13);
			this.label8.TabIndex = 15;
			this.label8.Text = "Opombe:";
			// 
			// i_upime
			// 
			this.i_upime.Location = new System.Drawing.Point(158, 76);
			this.i_upime.Name = "i_upime";
			this.i_upime.Size = new System.Drawing.Size(252, 20);
			this.i_upime.TabIndex = 16;
			// 
			// i_geslo
			// 
			this.i_geslo.Location = new System.Drawing.Point(158, 105);
			this.i_geslo.Name = "i_geslo";
			this.i_geslo.Size = new System.Drawing.Size(252, 20);
			this.i_geslo.TabIndex = 17;
			// 
			// i_email
			// 
			this.i_email.Location = new System.Drawing.Point(158, 134);
			this.i_email.Name = "i_email";
			this.i_email.Size = new System.Drawing.Size(252, 20);
			this.i_email.TabIndex = 18;
			// 
			// i_sec_q
			// 
			this.i_sec_q.Location = new System.Drawing.Point(158, 165);
			this.i_sec_q.Name = "i_sec_q";
			this.i_sec_q.Size = new System.Drawing.Size(252, 20);
			this.i_sec_q.TabIndex = 19;
			// 
			// i_sec_a
			// 
			this.i_sec_a.Location = new System.Drawing.Point(158, 198);
			this.i_sec_a.Name = "i_sec_a";
			this.i_sec_a.Size = new System.Drawing.Size(252, 20);
			this.i_sec_a.TabIndex = 20;
			// 
			// i_date_birth
			// 
			this.i_date_birth.Location = new System.Drawing.Point(158, 229);
			this.i_date_birth.Name = "i_date_birth";
			this.i_date_birth.Size = new System.Drawing.Size(252, 20);
			this.i_date_birth.TabIndex = 21;
			// 
			// i_notes
			// 
			this.i_notes.Location = new System.Drawing.Point(158, 262);
			this.i_notes.Multiline = true;
			this.i_notes.Name = "i_notes";
			this.i_notes.Size = new System.Drawing.Size(252, 121);
			this.i_notes.TabIndex = 22;
			// 
			// i_naziv
			// 
			this.i_naziv.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.i_naziv.Location = new System.Drawing.Point(135, 21);
			this.i_naziv.Name = "i_naziv";
			this.i_naziv.Size = new System.Drawing.Size(167, 29);
			this.i_naziv.TabIndex = 23;
			// 
			// edit_btn
			// 
			this.edit_btn.Location = new System.Drawing.Point(243, 404);
			this.edit_btn.Name = "edit_btn";
			this.edit_btn.Size = new System.Drawing.Size(75, 23);
			this.edit_btn.TabIndex = 24;
			this.edit_btn.Text = "Shrani";
			this.edit_btn.UseVisualStyleBackColor = true;
			this.edit_btn.Click += new System.EventHandler(this.edit_btn_Click);
			// 
			// delete_btn
			// 
			this.delete_btn.Location = new System.Drawing.Point(335, 404);
			this.delete_btn.Name = "delete_btn";
			this.delete_btn.Size = new System.Drawing.Size(75, 23);
			this.delete_btn.TabIndex = 25;
			this.delete_btn.Text = "Izbriši";
			this.delete_btn.UseVisualStyleBackColor = true;
			this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
			// 
			// EntryDetail
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(422, 442);
			this.Controls.Add(this.delete_btn);
			this.Controls.Add(this.edit_btn);
			this.Controls.Add(this.i_naziv);
			this.Controls.Add(this.i_notes);
			this.Controls.Add(this.i_date_birth);
			this.Controls.Add(this.i_sec_a);
			this.Controls.Add(this.i_sec_q);
			this.Controls.Add(this.i_email);
			this.Controls.Add(this.i_geslo);
			this.Controls.Add(this.i_upime);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "EntryDetail";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Podrobnosti vnosa";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EntryDetail_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox i_upime;
		private System.Windows.Forms.TextBox i_geslo;
		private System.Windows.Forms.TextBox i_email;
		private System.Windows.Forms.TextBox i_sec_q;
		private System.Windows.Forms.TextBox i_sec_a;
		private System.Windows.Forms.TextBox i_date_birth;
		private System.Windows.Forms.TextBox i_notes;
		private System.Windows.Forms.TextBox i_naziv;
		private System.Windows.Forms.Button edit_btn;
		private System.Windows.Forms.Button delete_btn;
	}
}