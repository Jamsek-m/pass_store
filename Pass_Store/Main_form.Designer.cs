﻿namespace Pass_Store
{
    partial class Main_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_form));
			this.new_entry_btn = new System.Windows.Forms.Button();
			this.add_user_btn = new System.Windows.Forms.Button();
			this.data_izpis = new System.Windows.Forms.DataGridView();
			this.NAZIV = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.UPIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.GESLO = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.EMAIL = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SEC_Q = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.SEC_A = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DATE_BIRTH = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.NOTES = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ID_STORE = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.about_btn = new System.Windows.Forms.LinkLabel();
			((System.ComponentModel.ISupportInitialize)(this.data_izpis)).BeginInit();
			this.SuspendLayout();
			// 
			// new_entry_btn
			// 
			this.new_entry_btn.Location = new System.Drawing.Point(561, 24);
			this.new_entry_btn.Name = "new_entry_btn";
			this.new_entry_btn.Size = new System.Drawing.Size(96, 23);
			this.new_entry_btn.TabIndex = 3;
			this.new_entry_btn.Text = "Dodaj nov vnos";
			this.new_entry_btn.UseVisualStyleBackColor = true;
			this.new_entry_btn.Click += new System.EventHandler(this.new_entry_btn_Click);
			// 
			// add_user_btn
			// 
			this.add_user_btn.Location = new System.Drawing.Point(40, 24);
			this.add_user_btn.Name = "add_user_btn";
			this.add_user_btn.Size = new System.Drawing.Size(90, 23);
			this.add_user_btn.TabIndex = 6;
			this.add_user_btn.Text = "Spremeni geslo";
			this.add_user_btn.UseVisualStyleBackColor = true;
			this.add_user_btn.Click += new System.EventHandler(this.add_user_btn_Click);
			// 
			// data_izpis
			// 
			this.data_izpis.AllowUserToAddRows = false;
			this.data_izpis.AllowUserToDeleteRows = false;
			this.data_izpis.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.data_izpis.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NAZIV,
            this.UPIME,
            this.GESLO,
            this.EMAIL,
            this.SEC_Q,
            this.SEC_A,
            this.DATE_BIRTH,
            this.NOTES,
            this.ID_STORE});
			this.data_izpis.Location = new System.Drawing.Point(40, 103);
			this.data_izpis.Name = "data_izpis";
			this.data_izpis.ReadOnly = true;
			this.data_izpis.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.data_izpis.Size = new System.Drawing.Size(617, 521);
			this.data_izpis.TabIndex = 7;
			this.data_izpis.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.data_izpis_CellClick);
			// 
			// NAZIV
			// 
			this.NAZIV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.NAZIV.HeaderText = "Naziv";
			this.NAZIV.Name = "NAZIV";
			this.NAZIV.ReadOnly = true;
			// 
			// UPIME
			// 
			this.UPIME.HeaderText = "Up. ime";
			this.UPIME.Name = "UPIME";
			this.UPIME.ReadOnly = true;
			this.UPIME.Visible = false;
			// 
			// GESLO
			// 
			this.GESLO.HeaderText = "Geslo";
			this.GESLO.Name = "GESLO";
			this.GESLO.ReadOnly = true;
			this.GESLO.Visible = false;
			// 
			// EMAIL
			// 
			this.EMAIL.HeaderText = "E-mail";
			this.EMAIL.Name = "EMAIL";
			this.EMAIL.ReadOnly = true;
			this.EMAIL.Visible = false;
			// 
			// SEC_Q
			// 
			this.SEC_Q.HeaderText = "Varnostno vprašanje";
			this.SEC_Q.Name = "SEC_Q";
			this.SEC_Q.ReadOnly = true;
			this.SEC_Q.Visible = false;
			// 
			// SEC_A
			// 
			this.SEC_A.HeaderText = "Odgovor na var. vpr.";
			this.SEC_A.Name = "SEC_A";
			this.SEC_A.ReadOnly = true;
			this.SEC_A.Visible = false;
			// 
			// DATE_BIRTH
			// 
			this.DATE_BIRTH.HeaderText = "Datum Rojstva";
			this.DATE_BIRTH.Name = "DATE_BIRTH";
			this.DATE_BIRTH.ReadOnly = true;
			this.DATE_BIRTH.Visible = false;
			// 
			// NOTES
			// 
			this.NOTES.HeaderText = "Opombe";
			this.NOTES.Name = "NOTES";
			this.NOTES.ReadOnly = true;
			this.NOTES.Visible = false;
			// 
			// ID_STORE
			// 
			this.ID_STORE.HeaderText = "Id";
			this.ID_STORE.Name = "ID_STORE";
			this.ID_STORE.ReadOnly = true;
			this.ID_STORE.Visible = false;
			// 
			// about_btn
			// 
			this.about_btn.AutoSize = true;
			this.about_btn.Location = new System.Drawing.Point(595, 638);
			this.about_btn.Name = "about_btn";
			this.about_btn.Size = new System.Drawing.Size(62, 13);
			this.about_btn.TabIndex = 8;
			this.about_btn.TabStop = true;
			this.about_btn.Text = "O programu";
			this.about_btn.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.about_btn_LinkClicked);
			// 
			// Main_form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(687, 669);
			this.Controls.Add(this.about_btn);
			this.Controls.Add(this.data_izpis);
			this.Controls.Add(this.add_user_btn);
			this.Controls.Add(this.new_entry_btn);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Main_form";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Shramba gesel";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.izhod_event);
			((System.ComponentModel.ISupportInitialize)(this.data_izpis)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button new_entry_btn;
        private System.Windows.Forms.Button add_user_btn;
		private System.Windows.Forms.DataGridView data_izpis;
		private System.Windows.Forms.DataGridViewTextBoxColumn NAZIV;
		private System.Windows.Forms.DataGridViewTextBoxColumn UPIME;
		private System.Windows.Forms.DataGridViewTextBoxColumn GESLO;
		private System.Windows.Forms.DataGridViewTextBoxColumn EMAIL;
		private System.Windows.Forms.DataGridViewTextBoxColumn SEC_Q;
		private System.Windows.Forms.DataGridViewTextBoxColumn SEC_A;
		private System.Windows.Forms.DataGridViewTextBoxColumn DATE_BIRTH;
		private System.Windows.Forms.DataGridViewTextBoxColumn NOTES;
		private System.Windows.Forms.DataGridViewTextBoxColumn ID_STORE;
		private System.Windows.Forms.LinkLabel about_btn;
	}
}