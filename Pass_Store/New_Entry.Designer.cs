﻿namespace Pass_Store
{
    partial class New_Entry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(New_Entry));
			this.i_naziv = new System.Windows.Forms.TextBox();
			this.groupBox = new System.Windows.Forms.GroupBox();
			this.i_notes = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.i_date_birth = new System.Windows.Forms.TextBox();
			this.i_sec_a = new System.Windows.Forms.TextBox();
			this.i_sec_q = new System.Windows.Forms.TextBox();
			this.i_email = new System.Windows.Forms.TextBox();
			this.i_geslo = new System.Windows.Forms.TextBox();
			this.i_upime = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.add_entry_btn = new System.Windows.Forms.Button();
			this.groupBox.SuspendLayout();
			this.SuspendLayout();
			// 
			// i_naziv
			// 
			this.i_naziv.Location = new System.Drawing.Point(190, 20);
			this.i_naziv.Name = "i_naziv";
			this.i_naziv.Size = new System.Drawing.Size(240, 20);
			this.i_naziv.TabIndex = 0;
			// 
			// groupBox
			// 
			this.groupBox.Controls.Add(this.i_notes);
			this.groupBox.Controls.Add(this.label8);
			this.groupBox.Controls.Add(this.label7);
			this.groupBox.Controls.Add(this.label6);
			this.groupBox.Controls.Add(this.label5);
			this.groupBox.Controls.Add(this.label4);
			this.groupBox.Controls.Add(this.label3);
			this.groupBox.Controls.Add(this.label2);
			this.groupBox.Controls.Add(this.i_date_birth);
			this.groupBox.Controls.Add(this.i_sec_a);
			this.groupBox.Controls.Add(this.i_sec_q);
			this.groupBox.Controls.Add(this.i_email);
			this.groupBox.Controls.Add(this.i_geslo);
			this.groupBox.Controls.Add(this.i_upime);
			this.groupBox.Location = new System.Drawing.Point(59, 77);
			this.groupBox.Name = "groupBox";
			this.groupBox.Size = new System.Drawing.Size(431, 368);
			this.groupBox.TabIndex = 15;
			this.groupBox.TabStop = false;
			this.groupBox.Text = "Podatki";
			// 
			// i_notes
			// 
			this.i_notes.Location = new System.Drawing.Point(171, 309);
			this.i_notes.Multiline = true;
			this.i_notes.Name = "i_notes";
			this.i_notes.Size = new System.Drawing.Size(252, 53);
			this.i_notes.TabIndex = 13;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(89, 312);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(50, 13);
			this.label8.TabIndex = 12;
			this.label8.Text = "Opombe:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(64, 265);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(75, 13);
			this.label7.TabIndex = 11;
			this.label7.Text = "Datum rojstva:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(32, 217);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(108, 13);
			this.label6.TabIndex = 10;
			this.label6.Text = "Odgovor na var. vpr.:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(32, 173);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(107, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "Varnostno vprašanje:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(101, 129);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(38, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "E-mail:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(102, 82);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(37, 13);
			this.label3.TabIndex = 7;
			this.label3.Text = "Geslo:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(50, 38);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(89, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Uporabniško ime:";
			// 
			// i_date_birth
			// 
			this.i_date_birth.Location = new System.Drawing.Point(171, 262);
			this.i_date_birth.Name = "i_date_birth";
			this.i_date_birth.Size = new System.Drawing.Size(252, 20);
			this.i_date_birth.TabIndex = 5;
			// 
			// i_sec_a
			// 
			this.i_sec_a.Location = new System.Drawing.Point(171, 214);
			this.i_sec_a.Name = "i_sec_a";
			this.i_sec_a.Size = new System.Drawing.Size(252, 20);
			this.i_sec_a.TabIndex = 4;
			// 
			// i_sec_q
			// 
			this.i_sec_q.Location = new System.Drawing.Point(171, 170);
			this.i_sec_q.Name = "i_sec_q";
			this.i_sec_q.Size = new System.Drawing.Size(252, 20);
			this.i_sec_q.TabIndex = 3;
			// 
			// i_email
			// 
			this.i_email.Location = new System.Drawing.Point(171, 126);
			this.i_email.Name = "i_email";
			this.i_email.Size = new System.Drawing.Size(252, 20);
			this.i_email.TabIndex = 2;
			// 
			// i_geslo
			// 
			this.i_geslo.Location = new System.Drawing.Point(171, 79);
			this.i_geslo.Name = "i_geslo";
			this.i_geslo.Size = new System.Drawing.Size(252, 20);
			this.i_geslo.TabIndex = 1;
			// 
			// i_upime
			// 
			this.i_upime.Location = new System.Drawing.Point(171, 35);
			this.i_upime.Name = "i_upime";
			this.i_upime.Size = new System.Drawing.Size(252, 20);
			this.i_upime.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(116, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(70, 13);
			this.label1.TabIndex = 16;
			this.label1.Text = "Prikazno ime:";
			// 
			// add_entry_btn
			// 
			this.add_entry_btn.Location = new System.Drawing.Point(218, 470);
			this.add_entry_btn.Name = "add_entry_btn";
			this.add_entry_btn.Size = new System.Drawing.Size(143, 23);
			this.add_entry_btn.TabIndex = 17;
			this.add_entry_btn.Text = "Dodaj vnos";
			this.add_entry_btn.UseVisualStyleBackColor = true;
			this.add_entry_btn.Click += new System.EventHandler(this.add_entry_btn_Click);
			// 
			// New_Entry
			// 
			this.AcceptButton = this.add_entry_btn;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(570, 524);
			this.Controls.Add(this.add_entry_btn);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.groupBox);
			this.Controls.Add(this.i_naziv);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "New_Entry";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Nov vnos";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.New_Entry_FormClosing);
			this.groupBox.ResumeLayout(false);
			this.groupBox.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox i_naziv;
        private System.Windows.Forms.GroupBox groupBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox i_notes;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox i_date_birth;
        private System.Windows.Forms.TextBox i_sec_a;
        private System.Windows.Forms.TextBox i_sec_q;
        private System.Windows.Forms.TextBox i_email;
        private System.Windows.Forms.TextBox i_geslo;
        private System.Windows.Forms.TextBox i_upime;
        private System.Windows.Forms.Button add_entry_btn;
    }
}