﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pass_Store {

	public partial class EntryDetail : Form {
		private string id;

		//konstruktor dobi vrednosti v main formi, in te podatke prikaže
		public EntryDetail(string naziv, string upime, string geslo, string email, string sec_q, string sec_a, string date_birth, string notes, string id_store) {
			InitializeComponent();
			i_naziv.Text = naziv;
			i_upime.Text = upime;
			i_geslo.Text = geslo;
			i_email.Text = email;
			i_sec_q.Text = sec_q;
			i_sec_a.Text = sec_a;
			i_date_birth.Text = date_birth;
			i_notes.Text = notes;
			id = id_store;
		}

		//shrani podatke v bazo (ne preverja sprememb)
		private void edit_btn_Click(object sender, EventArgs e) {
			Kripto enkript = new Kripto();
			string enc_naziv = enkript.Encrypt(i_naziv.Text, false);
			string enc_upime = enkript.Encrypt(i_upime.Text, false);
			string enc_geslo = enkript.Encrypt(i_geslo.Text, false);
			string enc_email = enkript.Encrypt(i_email.Text, false);
			string enc_sec_q = enkript.Encrypt(i_sec_q.Text, false);
			string enc_sec_a = enkript.Encrypt(i_sec_a.Text, false);
			string enc_date_birth = enkript.Encrypt(i_date_birth.Text, false);
			string enc_notes = enkript.Encrypt(i_notes.Text, false);

			string sql = "update SAVED_PASS set " +
				"DATE_BIRTH='" + enc_date_birth + "'," +
				"NAZIV='" + enc_naziv + "'," +
				"UPIME='" + enc_upime + "'," +
				"GESLO='" + enc_geslo + "'," +
				"EMAIL='" + enc_email + "'," +
				"SEC_Q='" + enc_sec_q + "'," +
				"SEC_A='" + enc_sec_a + "'," +
				"NOTES='" + enc_notes + "'" +
				"WHERE ID_STORE=" + id;

			ConnectDB baza = new ConnectDB();
			baza.izvediNonQuery(sql);
			baza.zapriPovezavo();
			pojdiNazaj();
		}

		//izbriše zapis iz baze
		private void delete_btn_Click(object sender, EventArgs e) {
			string sql = "delete from SAVED_PASS where ID_STORE=" + id;
			ConnectDB baza = new ConnectDB();
			baza.izvediNonQuery(sql);
			baza.zapriPovezavo();
			pojdiNazaj();
		}

		protected void pojdiNazaj() {
			this.Close();
		}

		private void EntryDetail_FormClosing(object sender, FormClosingEventArgs e) {
			var mainForm = new Main_form();
			mainForm.Show();
		}
	}
}
